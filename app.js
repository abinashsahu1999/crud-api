
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const { v4: uuidv4 } = require('uuid');
require('dotenv').config();
const app = express();
app.use(bodyParser.json());

mongoose.connect(process.env.MONGO_URI)
  .then(() => {
    console.log(`Sucessfully Connected To MongoDB`);
  })
  .catch((err) => {
    console.log(` Connection Failed ${err}`);
  })


const userSchema = new mongoose.Schema({
  id: { type: String, default: uuidv4 },
  username: { type: String, required: true },
  age: { type: Number, required: true },
  hobbies: { type: [String], default: [],required: true },
});

const User = mongoose.model('User', userSchema);


app.get('/api/users', async (req, res) => {
  try {
    const userList = await User.find();
    console.log("userList----->", userList);
    res.status(200).json(userList);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.get('/api/users/:userId', async (req, res) => {
  const userId = req.params.userId;

  if (!uuidv4(userId)) {
    res.status(400).json({ error: 'userId is invalid (not uuid)' });
    return;
  }

  try {
    const user = await User.findOne({ id: userId });
    if (!user) {
      res.status(404).json({ error: 'userId does not exits' });
      return;
    }
    console.log("user", user);
    res.status(200).json(user);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.post('/api/users', async (req, res) => {
  const { username, age, hobbies } = req.body;

  if (!username || !age || !hobbies) {
    res.status(400).json({ error: 'request body does not contain required fields ' });
    return;
  }

  try {
    const newUser = new User({ username, age, hobbies });
    const savedUser = await newUser.save();
    console.log(`<---savedUser----> ${savedUser}`);
    res.status(201).json(savedUser);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.put('/api/users/:userId', async (req, res) => {
  const userId = req.params.userId;

  if (!uuidv4(userId)) {
    res.status(400).json({ error: 'userId is invalid (not uuid)' });
    return;
  }

  try {
    const user = await User.findOne({ id: userId });
    if (!user) {
      res.status(404).json({ error: 'User not found' });
      return;
    }

    const { username, age, hobbies } = req.body;
    user.username = username || user.username;
    user.age = age || user.age;
    user.hobbies = hobbies || user.hobbies;

    const updatedUser = await user.save();
    console.log("updatedUser", updatedUser);
    res.status(200).json(updatedUser);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.delete('/api/users/:userId', async (req, res) => {
  const userId = req.params.userId;

  if (!uuidv4(userId)) {
    res.status(400).json({ error: 'userId is invalid (not uuid)' });
    return;
  }

  try {
    const user = await User.findOneAndDelete({ id: userId });
    if (!user) {
      res.status(404).json({ error: 'User not found' });
      return;
    }
    console.log("User deleted from DB---->",user);
    res.status(204).send('the record is found and deleted')
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});


app.use((req, res) => {
  res.status(404).json({ error: 'url is not hosted in our server' });
});

// Handle errors
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).json({ error: 'Internal Server Error' });
});


const port = process.env.PORT;
app.listen(port, () => {
  console.log(`Server is running on port http://localhost:${port}`);
});
